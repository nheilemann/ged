<?php

namespace Shopware\Themes\GEDShopTheme;

use Doctrine\Common\Collections\ArrayCollection;
use Shopware\Components\Form as Form;
use Shopware\Components\Theme\ConfigSet;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';

    protected $name = <<<'SHOPWARE_EOD'
Theme für den GED-Onlineshop
SHOPWARE_EOD;

    protected $description = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
Nimbits
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

	private $themeColorDefaults = [
        'brand-primary' => '#00468e',
		'font-headline-stack' => '"Babas Neue", "Open Sans", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;',
	];
	
	protected $javascript = [
		'src/js/theme.js'
	];

    public function createConfig(Form\Container\TabContainer $container)
    {
    }
	
	public function createConfigSets(ArrayCollection $collection)
    {
        $set = new ConfigSet();
        $set->setName('GED (Standard)')->setDescription(
            'in den Farben vom GED Shop'
        )->setValues($this->themeColorDefaults);
        $collection->add($set);
		
        $collection->add($set);
    }
}