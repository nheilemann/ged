// Sticky header
$(function () {

	var theme_header = $('header');
	var theme_navigation = $('.navigation-main');
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 40) {
			theme_header.removeClass('unscrolled').addClass("scrolled");
			theme_navigation.removeClass('unscrolled').addClass("scrolled");
		}
		else {
			theme_header.removeClass("scrolled").addClass('unscrolled');
			theme_navigation.removeClass("scrolled").addClass('unscrolled');
		}
	});
});


// Add class when cart is opend
$.subscribe('plugin/swCollapseCart/onLoadCartFinished', function()
{
	$( ".js--overlay" ).addClass( "is--cart-opend" );
	$( ".navigation-main" ).addClass( "is--cart-opend" );
});

$.subscribe('plugin/swOffcanvasMenu/onCloseMenu ', function()
{
	$( ".js--overlay" ).removeClass( "is--cart-opend" );
	$( ".navigation-main" ).removeClass( "is--cart-opend" );
});